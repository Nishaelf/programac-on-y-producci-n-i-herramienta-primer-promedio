﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Floor))]
public class FloorEditor : Editor {

    private int numeroDePisos;
    

    public override void OnInspectorGUI() {
        Floor myTarget = (Floor)target;

        DrawDefaultInspector();

        numeroDePisos = EditorGUILayout.IntField("Numero de Pisos", numeroDePisos);

     

        if (GUILayout.Button("Create Building")) {
            Vector3 origin = myTarget.transform.position;
            myTarget.CreateFloor(myTarget.nSpaces);

            for (int i = 0; i < numeroDePisos - 1; i++) {
                myTarget.transform.position = new Vector3(myTarget.transform.position.x, myTarget.transform.position.y + myTarget.heightFloor, myTarget.transform.position.z);
                myTarget.CreateFloor(myTarget.nSpaces);
            }

            GameObject temp = GameObject.CreatePrimitive(PrimitiveType.Cube);
            temp.transform.localScale = new Vector3(0.5f, myTarget.heightFloor * numeroDePisos, 0.5f);
            temp.transform.position = new Vector3(origin.x, temp.transform.localScale.y / 2, origin.z);
            myTarget.transform.position = origin;
            temp.name = "Stairs";
        }
    }
}