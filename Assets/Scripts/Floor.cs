﻿using UnityEngine;

public class Floor : MonoBehaviour {

    [Range(10f, 20f)]
    public float dimentionPlane;

    [Range(1, 5)]
    public int nSpaces;

    public GameObject temp;

    public float heightFloor;

    public void CreateFloor(int spaces) {
        GameObject goMain = new GameObject("Piso");
        for (int i = 0; i < spaces; i++) {

            Object.Instantiate(temp);
          

            Vector3 size = Vector3.one;
            size.x = Random.Range(1.5f, 7f);
            size.y = heightFloor;
            size.z = Random.Range(1.5f, 7f);

            temp.transform.localScale = size;

            float distan = dimentionPlane / 2;
            float x = Random.Range(-distan + size.x / 2, distan - size.x / 2);
            float y = transform.position.y + heightFloor / 2;
            float z = Random.Range(-distan + size.z / 2, distan - size.z / 2);

            Vector3 pos = new Vector3(x, y, z);

            temp.transform.position = pos;
            temp.transform.SetParent(goMain.transform);
            temp.name = "Habitación";
        }
    }

    private void OnDrawGizmosSelected() {
        Vector3 size = new Vector3(1, 0, 1) * dimentionPlane;
        Gizmos.DrawWireCube(transform.position, size);
    }
}